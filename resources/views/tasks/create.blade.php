@extends('layouts.app')

@section('content')

<div class="card text-center">
    <div class="card-header">
        <h3 class="mt-2">Add Task</h3>
    </div>
    <div class="card-body">
    <form action="{{ route('tasks.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <div class="row">
                <div class="col-md-2 mt-1">
                <label for="title"><h3>Title</h3></label>
                </div>
                <div class="col-md-10">
                <input type="text"
                value="{{ old('title') }}"
                class="form-control @error('title') is-invalid @enderror"
                name="title" id="title">
                @error('title')
                    <p class="text-danger">{{ $message }}</p>
                @enderror
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                    <div class="col-md-2 mt-1">
                        <label for="body"><h3>Body</h3></label>
                    </div>
                    <div class="col-md-10">
                        <input type="text"
                        value="{{ old('body') }}"
                        class="form-control @error('body') is-invalid @enderror"
                        name="body" id="body">
                        @error('body')
                            <p class="text-danger">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-2 mt-4">
                    <label for="users"><h3>Members</h3></label>
                </div>
                <div class="col-md-10">
                    <select name="users" id="users" class="form-control select2">
                    @foreach($users as $user)
                    @if($user->role === 'member')
                        <option value="{{$user->id}}">{{$user->name}}
                        </option>
                    @endif
                        @endforeach
                        </select>
                    @error('user')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-md-2 mt-1">
                    <label for="deadline"><h3>Deadline</h3></label>
                </div>
                <div class="col-md-10">
                    <input type="date"
                    value="{{ old('deadline') }}"
                    class="form-control @error('deadline') is-invalid @enderror"
                    name="deadline" id="deadline">
                    @error('deadline')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
            </div>
        </div>
        <div class="form-group">
            <button class="btn btn-success" type="submit">Add Task</button>
        </div>
    </form>
    </div>
</div>

@endsection