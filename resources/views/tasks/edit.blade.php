@extends('layouts.app')

@section('content')

<div class="card">
        <div class="card-header">
            Edit Task
        </div>

        <div class="card-body">
            <form action="{{ route('tasks.update', $task) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text"
                    value="{{ old('title', $task->title) }}"
                    class="form-control @error('title') is-invalid @enderror"
                    name="title" id="title">
                    @error('title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="excerpt">Body</label>
                    <textarea
                    class="form-control @error('body') is-invalid @enderror"
                    name="body" id="body" rows="4">{{ old('body', $task->body) }}</textarea>
                    @error('body')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Members</label>
                    <select name="user_id" id="user_id" class="form-control select2">
                    @foreach($users as $user)
                    @if($user->role === 'member')
                        <option value="{{old('user_id', $user->id)}}">{{$user->name}}
                        </option>
                    @endif
                        @endforeach
                        </select>
                    @error('user_id')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="deadline">Deadline</label>
                    <input type="date"
                    value="{{ old('deadline', $task->deadline)}}"
                    class="form-control @error('deadline') is-invalid @enderror"
                    name="deadline" id="deadline">
                    @error('deadline')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Update Task</button>
                </div>
            </form>
        </div>
    </div>

@endsection