@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('content')
<div class="row">
    @foreach($users as $user)
        @if($user->tasks->count() > 0 && auth()->user()->role === 'leader' && $user->role != auth()->user()->role )
        <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <div class="row mt-2">
                    <div class="col-md-12 text-center">
                        <h5>
                        @foreach($user->tasks as $userTask)
                        @if($user->is_completed_value($userTask,$userTask->member->id) === $user->tasks->count())
                        <?php $statusFlag = true; ?>
                        @endif
                        @endforeach
                        @if($statusFlag)
                            <i class="fa fa-check"></i>
                        @endif
                        {{$user->name}}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @foreach($tasks as $task)
                @if($user->table($task)->member->id === $user->id && ($user->table($task)->leader->id !== auth()->user()->id))
                <!-- <p>H</p> -->
                @endif
                    @if($user->name === $task->member->name && $task->leader_id === auth()->user()->id)
                    <div class="card-header mt-2">
                        <p>
                        @if($task->status === 'completed')
                            <i class="fa fa-check"></i>
                        @endif
                        <a href="{{route('tasks.show', $task)}}">{!! Str::limit($task->title, 25) !!}</a>
                        </p>
                    </div>
                    @endif
                @endforeach
            </div>
            <div class="card-header text-center">
                <a href="{{ route('tasks.create') }}">
                <i class="fa fa-plus"></i>
                Add Another task
                </a>
            </div>
        </div>
        </div>
        @endif
    @endforeach
    @foreach($users as $user)
    @if($user->tasks->count() > 0 && auth()->user()->role !== 'leader' && $user->id === auth()->user()->id)
        <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <div class="row mt-2">
                    <div class="col-md-12 text-center">
                        <h5>
                        @foreach($user->tasks as $userTask)
                            @if($user->is_completed($userTask) === $user->tasks->count() && $user->name === auth()->user()->name)
                                <?php $flag = true; ?>
                            @endif
                        @endforeach
                        @if($flag)
                        <i class="fa fa-check"></i>
                        @endif
                        {{auth()->user()->name}}
                        </h5>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @foreach($tasks as $task)
                @if(auth()->user()->name === $task->member->name)
                <div class="card-header mt-2">
                    <p>
                    @if($task->status === 'completed')
                        <i class="fa fa-check"></i>
                    @endif
                    <a href="{{route('tasks.show', $task)}}">
                    {!! Str::limit($task->title, 25) !!}
                    </a>
                    </p>
                    </div>
                @endif
                @endforeach
            </div>
        </div>
        </div>
    @endif
    @endforeach
</div>

@endsection