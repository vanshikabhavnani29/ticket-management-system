@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12"> 
            <div class="card">
                <div class="card-header">
                    <h3>{{$task->title}}</h3>
                </div>
                <div class="card-body">
                    <p>{{$task->body}}</p>
                    <p class="text-muted">Deadline: {!! Str::limit($task->deadline, 11) !!}</p>
                    <p class="text-muted">Assigned : {{$task->created_date}}</p>
                    <p class="text-muted">Task For : {{$task->member->name}}</p>
                    <p class="text-muted">Status : {{$task->status}}</p>
                    @if(auth()->user()->role === 'leader')
                        <a href="{{route('tasks.edit', $task)}}" class="btn btn-success">
                        <i class="fa fa-edit"></i>
                        Edit
                        </a>
                        <a href="" class="btn btn-danger" onclick="displayModalForm({{ $task }})"
                            data-toggle="modal" data-target="#deleteModal">
                        <i class="fa fa-trash"></i>
                        Trash
                        </a>
                        @if($task->status === 'resolving')
                        <form action="{{route('tasks.completed', $task)}}" method="POST" style="display: inline-block">
                        @csrf
                        <button type="submit" class="btn btn-primary">Resolve</button>
                        </form>
                        <form action="{{route('tasks.incomplete', $task)}}" method="POST" style="display: inline-block">
                        @csrf
                        <button type="submit" class="btn btn-primary">UnResolve</button>
                        </form>
                        @endif
                        <a href="{{route('tasks.trashed')}}" class="btn btn-danger" >
                        <i class="fa fa-trash"></i>
                        View Trashed Tasks
                        </a>
                        <a href="{{route('tasks.index')}}" class="btn btn-primary" >
                        View Tasks
                        </a>
                    @elseif(auth()->user()->role === 'member')
                        <p class="text-muted">Assigned By : {{$task->leader->name}}</p>
                        <form action="{{route('tasks.completed', $task)}}" method="POST" style="display:inline-block">
                        @csrf
                        @if($task->status === 'resolving')
                            <p style="color: green">Resolving...</p>
                        @elseif($task->status === 'completed')
                            <p style="color: green">Completed</p>
                        @elseif($task->status === 'incomplete')
                        <button type="submit" class="btn btn-outline-primary">Mark As Completed</button>
                        @endif
                    </form>
                    <a href="{{route('tasks.index')}}" class="btn btn-primary" >
                        View Tasks
                    </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                </div>
            <div class="modal-body">
            <form action="" method="POST" id="deleteForm">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    <p>Are you sure you wanna trash?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-danger">
                        Trash
                    </button>
                </div>
            </form>
            </div>
            </div>
    </div>

@endsection

@section('page-level-scripts')
    <script>
        function displayModalForm($task){
            var url = '/trash/'+$task.id;
            $("#deleteForm").attr('action', url);
        }
    </script>
@endsection