@extends('layouts.app')
@section('content')
    <div class="d-flex justify-content-end mb-3 mt-3">
        <a href="{{ route('tasks.create') }}" class="btn btn-primary">Add Tasks</a>
    </div>
    <div class="card">
        <div class="card-header">
            Tasks
        </div>
        <div class="card-body">
        @if($tasks->count() > 0)
            <table class="table table-bordered">
                <thead>
                    <th>Title</th>
                    <th>Body</th>
                    <th>Deadline</th>
                    <th>Assigned</th>
                    <th>Member</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach($tasks as $task)
                    <tr>
                        <td>{{$task->title}}</td>
                        <td>{{$task->body}}</td>
                        <td>{!! Str::limit($task->deadline, 11) !!}</td>
                        <td>{{$task->created_date}}</td>
                        <td>{{$task->member->name}}</td>
                        <td>
                            <form action="{{route('tasks.restore', $task->id)}}" method="POST">
                                @csrf
                                @method('PUT')
                                <button class="btn btn-primary btn-sm" type="submit">Restore</button>
                            </form>
                            <a href="" class="btn btn-danger btn-sm"
                            onclick="displayModalForm({{$task}})"
                            data-toggle="modal"
                            data-target="#deleteModal">Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else
                <h5>Nothing to delete</h5>
            @endif
        </div>
        <div class="card-footer">
            {{ $tasks->links() }}
        </div>
    </div>
    
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                </div>
            <div class="modal-body">
            <form action="" method="POST" id="deleteForm">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    <p>Are you sure you wanna delete</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-danger">
                        Delete Task
                    </button>
                </div>
            </form>
            </div>
            </div>
    </div>
@endsection
@section('page-level-scripts')
    <script type="text/javascript">
        function displayModalForm($task){
            var url = '/tasks/'+$task.id;
            $("#deleteForm").attr('action', url);
        }
    </script>
@endsection