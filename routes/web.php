<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('tasks', 'TasksController');
Route::delete('/trash/{task}', 'TasksController@trash')->name('tasks.trash');
Route::get('/trashed', 'TasksController@trashed')->name('tasks.trashed');
Route::put('/restore/{task}', 'TasksController@restore')->name('tasks.restore');
Route::post('tasks/{task}/completed', 'StatusController@update')->name('tasks.completed');
Route::post('tasks/{task}/incomplete', 'StatusController@updateIncomplete')->name('tasks.incomplete');
// Route::get('tasks/{slug}', 'TasksController@show')->name('tasks.show');
