<?php

namespace App\Http\Controllers;

use App\Task;

use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function update(Request $request, Task $task){
        if($task->status === 'incomplete' && $task->user_id === $task->member->id)
        {
            $task->update(['status' => 'resolving']);
            session()->flash('success', 'Task Completed, sent for resolving!');
            return redirect(route('tasks.show', $task));
        }
        if($task->status === 'resolving' && $task->leader_id === $task->leader->id)
        {
            $task->update(['status' => 'completed']);
            session()->flash('success', 'Task Completed!');
            return redirect(route('tasks.show', $task));
        }
    }
    public function updateIncomplete(Task $task){
        if($task->status === 'resolving'){
            $task->update(['status' => 'incomplete']);
            session()->flash('success', 'Task unresolved!');
            return redirect(route('tasks.show', $task));
        }
    }
}
