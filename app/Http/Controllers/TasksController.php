<?php

namespace App\Http\Controllers;

use App\Http\Requests\Tasks\CreateTaskRequest;
use App\Http\Requests\Tasks\UpdateTaskRequest;

use Illuminate\Support\Str;

use App\Task;
use App\User;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware(['auth'])->only(['create', 'store', 'edit', 'update', 'index']);
    }

    public function index()
    {
        // if(Gate::allows('show-task', $task)) {
            $users = User::all();
            $tasks = Task::all();
            $flag = false;
            $statusFlag = false;
            return view('tasks.index', compact([
            'statusFlag',
            'flag',
            'users',
            'tasks'
        ]));
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // if(auth()->user()->can('add-task', $task)) {
            $users = User::all();
            $tasks = Task::all();
            return view('tasks.create', compact([
                'users',
                'tasks'
        ]));
        // }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slug = $request->title;
        $task = Task::create([
            'title'=>$request->title,
            'slug' => Str::slug($slug),
            'body'=>$request->body,
            'user_id'=>$request->users,
            'leader_id' => auth()->user()->id,
            'deadline'=>$request->deadline
        ]);

        // dd($request);
        // dd($request->tags);

        session()->flash('success', 'Task Created Successfully');
        
        return redirect(route('tasks.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        return view('tasks.show', compact([
            'task'
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        $users = User::all();
        return view('tasks.edit', compact([
            'users',
            'task'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $data= $request->only(['title', 'body', 'user_id', 'deadline']);
        // dd($data);
        $task->update($data);


        session()->flash('success', 'Task Updated Successfully');
        
        return redirect(route('tasks.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::onlyTrashed()->findOrFail($id);
        $task->forceDelete();
        session()->flash('success', 'Task Deleted Successfully');
        return redirect()->back();

    }

    public function trash(Task $task)
    {
        $task->delete();
        session()->flash('success', 'Task Trashed Successfully');
        return redirect(route('tasks.index'));
    }

    public function trashed()
    {
        // $trashed = DB::table('tasks')->whereNotNull('deleted_at');
        $trashed = Task::onlyTrashed()->paginate(1);
        return view('tasks.trashed')->with('tasks', $trashed);
    }

    public function restore($id){
        $trashedTask = Task::onlyTrashed()->findOrFail($id);
        $trashedTask->restore();
        session()->flash('success', 'Task restored Successfully');
        return redirect(route('tasks.index'));    
    }
}
