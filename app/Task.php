<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Task extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $dates = ['deadline', 'updated_at', 'created_at'];
    protected $appends = ['diffInDays'];
    protected $fillable = [
        'title', 'body', 'user_id', 'slug', 'deadline', 'status', 'leader_id'
    ];
    public function getDiffInDaysAttribute()
    {
        return $this->deadline->diffInDays($this->updated_at);
    }
    public function setTitleAttribute($title){
        $this->attributes['title'] = $title;
        $this->attributes['slug'] = Str::slug($title);
    }
    
    public function getUrlAttribute(){
        return "tasks/{$this->slug}";
    }

    public function getCreatedDateAttribute(){
        return $this->created_at->diffForHumans();
    }


    public function status(){
        return $this->belongsTo(Task::class);
    }

    public function member(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function leader(){
        return $this->belongsTo(User::class, 'leader_id');
    }
}
