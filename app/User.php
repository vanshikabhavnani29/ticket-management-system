<?php

namespace App;
use App\Task;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function tasks(){
        return $this->hasMany(Task::class, 'user_id');
    }
    // public function completed(){
    //     return $this->tasks()->where(['status'=> 'completed'])->count();
    // }
    public function table($tableName){
        $this->table = $tableName;
        return $this->table;
    }
    public function is_completed_value($tableName, $value)
    {
        return $this->table($tableName)->where([
            'status' => 'completed',
            'user_id' => $value,
            ])->count();
    }
    public function is_completed($tableName)
    {
        return $this->table($tableName)->where([
            'status' => 'completed'
            ])->count();
    }
    public function isBest()
    {
        return $this->taskTable->diffInDays;
    }
}
