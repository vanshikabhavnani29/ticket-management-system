<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
function array_randomRole($array, $amount = 1)
    {
        $keys = array_rand($array, $amount);
        if($amount == 1){
            return $array[$keys];
        }
        $results = [];
        foreach($keys as $key){
            $results[] = $array[$key];
        }
        return $results;
    }
$factory->define(User::class, function (Faker $faker) {
    $role = ['leader', 'member'];
    return [
        'name' => $this->faker->name,
        'email' => $this->faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'role' => array_randomRole($role),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});
