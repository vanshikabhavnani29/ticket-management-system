<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Task;
use App\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/
function array_random($array, $amount = 1)
            {
                $keys = array_rand($array, $amount);
                if($amount == 1){
                    return $array[$keys];
                }
                $results = [];
                foreach($keys as $key){
                    $results[] = $array[$key];
                }
                return $results;
            }

        $factory->define(Task::class, function (Faker $faker) {
            $users = User::all();
            foreach($users as $user){
                if($user->role === 'member')
                {
                    $member_id[] = $user->id;
                }
                else{
                    $leader_id[] = $user->id;
                }
            }
            return [
                'title'=> rtrim($this->faker->sentence(rand(5, 10)), '.'),
                'body'=>$this->faker->sentence(rand(10,18)),
                'user_id'=>array_random($member_id),
                'leader_id'=>array_random($leader_id),
                'deadline'=>\Carbon\Carbon::now()->format('Y-m-d')
            ];
        });


