<?php
 
namespace Database\Seeders;
 
use App\Models\Model;
use Illuminate\Database\Seeder;
 
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // factory(\App\User::class, 5)->create();
        factory(\App\Task::class, 5)->create();
        // factory(\App\User::class, 5)->create()->each(function ($user){
        //     $user->tasks()->saveMany(
        //         factory(\App\Task::class, rand(2,5))->make()
        //     );
        // });]]]]]]]]]]]]]]]]]]]]]]]]
    }
}

