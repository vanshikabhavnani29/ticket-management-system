<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('body');
            $table->enum('status', ['completed', 'incomplete', 'resolving'])->default('incomplete');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('leader_id');
            $table->timestamps();
            $table->timestamp('deadline')->default(now());
            $table->softDeletes();
            $table->foreign('leader_id')->references('id')->on('users')->where(['users.role' => 'leader'])->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->where(['users.role' => 'member'])->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
